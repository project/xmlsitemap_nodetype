INTRODUCTION
------------

The XML Sitemap Per Node Type module provides the additional context
for XML sitemap module that allows creating sitemaps per content type.

REQUIREMENTS
------------

This module requires the following modules:

 * XML sitemap (https://drupal.org/project/xmlsitemap)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Create or edit the existing XML sitemap on
   Administration » Configuration » Search and metadata » XML sitemap

 * Specify Node Types that should be included to the sitemap.
   Check none or all to include all.

 * Note: Only content types you enabled for XML sitemap inclusion
   will be included to the sitemap.

 * (optional) By default the path to the sitemap will be
   sitemap.xml if all content types are chosen and
   sitemap.xml/nodetype/machine_name_1-machine_name_2-... if few are chosen.
   Having Path module enabled you may create the aliases for the sitemaps on
   Administration » Configuration » Search and metadata » URL aliases »
   Add alias
   Use the above paths as the existing system paths.
